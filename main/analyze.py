import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import javalang
import pickle
import os
import sys
import heapq
from apted import APTED, PerEditOperationConfig
from zss import simple_distance
from tree_utils import visualize_tree
from tree_utils import print_tree
from tree_utils import gen_zss_tree
from tree_utils import gen_ast_tree
from tree_utils import gen_apted_tree
from tree_utils import get_invoked_methods
from tree_utils import node_count
from tree_utils import gen_dft
from tree_utils import javalang_node_count
from tree_utils import verify_tree
from plot_utils import plot_boxplot
from plot_utils import plot_bargraph
from plot_utils import plot_horizontal_bargraphs
from models import Project
from models import UnitTest


def load_data(input_file, output_file, overwrite=False, project_filter=None):
    sys.setrecursionlimit(10000)
    cycle_count = 0
    if os.path.exists(output_file):
        if overwrite == False:
            # load dictionaries directly from .pickle files
            with open(output_file, "rb") as f:
                projects, tests = pickle.load(f)
            storage = os.path.getsize(output_file)
            print('{} size={}'.format(output_file, storage))
            return projects, tests
        else:
            # remove output file before generating new .pickle files
            os.remove(output_file)
    # load csv from input file
    df = pd.read_csv(input_file)
    projects = dict()
    tests = dict()
    limit = float('inf')
    limit_count = 0
    for index, row in df.iterrows():
        if limit_count >= limit:
            break
        _id = row['id']
        raw = row['raw']
        is_flaky = row['is_flaky']
        test_metadata = _id.split(':')
        category = row['category']
        project_name = test_metadata[0]

        # debug
        if project_filter != None and project_name not in project_filter:
            continue
        limit_count += 1

        # add project to dictionary
        if project_name not in projects:
            # create new project here
            projects[project_name] = Project(
                name=project_name,
            )
        project = projects[project_name]
        full_name = test_metadata[1]
        parseable_str = UnitTest.build_parseable(source_code=raw)
        try:
            javalang_tree = gen_ast_tree(source_code=parseable_str)
        except:
            print(project.name)
            print(full_name, parseable_str)
            continue
        zss_tree = gen_zss_tree(
            source_code=parseable_str,
            javalang_tree=javalang_tree,
        )

        if zss_tree == None:
            continue
        elif verify_tree(zss_tree, set()) == False:
            # visualize_tree(zss_tree, full_name)
            cycle_count += 1
            continue

        apted_tree = gen_apted_tree(
            source_code=parseable_str,
            zss_tree=zss_tree,
        )

        if apted_tree == None:
            continue
        elif verify_tree(apted_tree, set()) == False:
            # visualize_tree(apted_tree, full_name)
            cycle_count += 1
            continue
        
        # generate traversal of tree
        dft_list = gen_dft(apted_tree)
        
        num_nodes = node_count(zss_tree, visited=set())
        invoked_methods = get_invoked_methods(javalang_tree)
        test = UnitTest(
            full_name=full_name,
            is_flaky=is_flaky,
            category=category,
            raw=raw,
            project_name=project.name,
            javalang_tree=javalang_tree,
            zss_tree=zss_tree,
            apted_tree=apted_tree,
            num_nodes=num_nodes,
            invoked_methods=invoked_methods,
            dft_list=dft_list,
        )
        tests[test._id] = test
        project.add_test(test)


    with open(output_file, "wb") as f:
        pickle.dump((projects,tests), f)
    storage = os.path.getsize(output_file)
    print('{} size={}'.format(output_file, storage))
    print('cyclic: {}'.format(cycle_count))
    return projects, tests

def zss_edit_distance(x, y, dist_cache, curr_heap):
    key1 = x._id + '-' + y._id
    key2 = y._id + '-' +  x._id
    if key1 in dist_cache:
        return dist_cache[key1]
    elif key2 in dist_cache:
        return dist_cache[key2]

    dist = simple_distance(x.zss_tree, y.zss_tree)
    dist_cache[key1] = dist
    dist_cache[key2] = dist
    return dist

def apted_edit_distance(x, y, dist_cache):
    key1 = x._id + '-' + y._id
    key2 = y._id + '-' +  x._id

    if key1 in dist_cache:
        return dist_cache[key1]
    elif key2 in dist_cache:
        return dist_cache[key2]
    apted = APTED(x.apted_tree, y.apted_tree, PerEditOperationConfig(.1, .1, .1))
    dist = apted.compute_edit_distance()
    dist_cache[key1] = dist
    dist_cache[key2] = dist
    return dist


def longest_common_subsequence(t1, t2, memo=None, lcs_cache=None):
    if hasattr(t1, '_id') and hasattr(t2, '_id'):
        key1 = t1._id + '-' + t2._id
        key2 = t2._id + '-' +  t1._id
        if lcs_cache != None:
            if key1 in lcs_cache:
                return lcs_cache[key1]
            elif key2 in lcs_cache:
                return lcs_cache[key2]
    dft1 = t1.dft_list
    dft2 = t2.dft_list
    if memo == None:
        memo = [list() for i in range(len(dft1)+1)]
        for i in range(len(dft1)+1):
            memo[i] = [0] * (len(dft2)+1)    
      
    for i in range(1, len(memo)):
        for j in range(1, len(memo[i])):
            diag = memo[i-1][j-1]
            left = memo[i][j-1]
            up = memo[i-1][j]
            _max = max(diag, max(left, up))
            if dft1[i-1] == dft2[j-1]:
                memo[i][j] = _max + 1
            else:
                memo[i][j] = _max
    
    if key1 and key2 and lcs_cache != None:
        lcs_cache[key1] = memo[-1][-1]
        lcs_cache[key2] = memo[-1][-1]

    return memo[-1][-1]

# def find_longest_subsequences(t1, t2):
#     memo = [list() for i in range(len(t1)+1)]
#     for i in range(len(t1)+1):
#         memo[i] = [0] * (len(t2)+1)
#     longest_sub_sequence(t1, t2, memo)
#     results = []
#     i = len(t1) - 1
#     i = len(t2) - 1
#     while i >= 0 and j >= 0:

def plot_num_nodes(dataset, projects,
                     category_filter=None, flaky_test_req=0):
    flaky = []
    nonflaky = []
    project_names = []
    for project in projects.values():
        # collect data based for each test in project
        flaky_data = []
        nonflaky_data = []
        for test in project.tests.values():
            if test.is_flaky:
                if category_filter and len(test.category) > 0:
                    if test.category not in category_filter:
                        continue
                flaky_data.append(test.num_nodes)
            else:            
                nonflaky_data.append(test.num_nodes)
        # store data in master list
        if len(flaky_data) > flaky_test_req:
            flaky.append(flaky_data)
            nonflaky.append(nonflaky_data)
            project_names.append(project.name)
        
    # plot data
    plot_boxplot(
        project_names=project_names,
        left_data=flaky,
        right_data=nonflaky,
        title='{}: pruned AST Node Count'.format(dataset),
        ylabel='node count'
    )

def plot_num_methods(dataset, projects, 
                    unique=True, category_filter=None, 
                    flaky_test_req=0):
    flaky = []
    nonflaky = []
    project_names = []
    for project in projects.values():
        # collect data based for each test in project
        flaky_data = []
        nonflaky_data = []
        for test in project.tests.values():
            if unique:
                num_methods = len(test.invoked_methods_set)  
            else:
                num_methods = len(test.invoked_methods)
            if test.is_flaky:
                if category_filter and len(test.category) > 0:
                    if test.category not in category_filter:
                        continue
                flaky_data.append(num_methods)
            else:            
                nonflaky_data.append(num_methods)
        # store data in master list
        if len(flaky_data) > flaky_test_req:
            flaky.append(flaky_data)
            nonflaky.append(nonflaky_data)
            project_names.append(project.name)
    
    # plot data
    plot_boxplot(
        project_names=project_names,
        left_data=flaky,
        right_data=nonflaky,
        title='{}: Number of invoked methods (unique={})'.format(dataset, unique),
        ylabel='# invoked methods'
    )

def plot_flaky_tests(dataset, projects, category_filter=None, flaky_test_req=0):
    flaky = []
    nonflaky = []
    project_names = []
    for project in projects.values():
        flaky_data = sum([1 if test.is_flaky else 0 for test in project.tests.values()])
        nonflaky_data = len(project.tests) - flaky_data
        # store data in master list
        if flaky_data > flaky_test_req:
            flaky.append(flaky_data)
            nonflaky.append(nonflaky_data)
            project_names.append(project.name)
    
    plot_bargraph(
        project_names=project_names,
        flaky=flaky,
        nonflaky=nonflaky,
        title='{}: Number of Flaky vs Non-flaky Tests'.format(dataset),
        ylabel='# tests'
    )

def plot_top_n_methods(dataset, tests, n=25, 
                    unique=False, category_filter=None):
    flaky, nonflaky = dict(), dict()
    for test in tests.values():
        if test.is_flaky:
            if category_filter != None:
                if test.category not in category_filter:
                    continue
        methods = test.invoked_methods_set if unique else test.invoked_methods
        for method in methods:
            if test.is_flaky:
                if method not in flaky:
                    flaky[method] = 0
                flaky[method] += 1
            else:
                if method not in nonflaky:
                    nonflaky[method] = 0
                nonflaky[method] += 1
    flaky_keys = sorted(flaky, key=flaky.get, reverse=True)
    nonflaky_keys = sorted(nonflaky, key=nonflaky.get, reverse=True)
    flaky_data = [(k, flaky[k]) for k in flaky_keys[:n]]
    nonflaky_data = [(k, nonflaky[k]) for k in nonflaky_keys[:n]]
    plot_horizontal_bargraphs(
        flaky=flaky_data,
        nonflaky=nonflaky_data,
        xlabel='# invocations',
        title='{}: Top {} Methods'.format(dataset,n),
    )
    return flaky_data, nonflaky_data

def calc_average_distance(curr_test, tests, dist_cache, category_filter=None):
    _sum = 0
    for other_test in tests:
        if curr_test._id == other_test._id:
            continue
        # _sum += apted_edit_distance(
        #         x=curr_test, 
        #         y=other_test, 
        #         dist_cache=dist_cache)
        _sum += 1 - (longest_common_subsequence(
                t1=curr_test, 
                t2=other_test, 
                lcs_cache=dist_cache)/other_test.num_nodes)
    return _sum / (len(tests) - 1)
        
def plot_average_distances(dataset, projects, category_filter=None, 
                        flaky_test_req=0, flaky_limit=200,
                        nonflaky_limit=float('inf'), test_limit=float('inf')):
    flaky = []
    nonflaky = []
    project_names = []
    for project in projects.values():
        print('project: {}'.format(project.name))
        dist_cache = dict()
        tests = project.tests.values()
        if len(tests) > test_limit:
            continue
        flaky_tests = list(filter(lambda t: t.is_flaky, tests))
        if len(flaky_tests) < flaky_test_req or len(flaky_tests) > flaky_limit:
            continue
        nonflaky_tests = list(filter(lambda t: t.is_flaky == False, tests))
        if len(nonflaky_tests) > nonflaky_limit:
            continue
        flaky_data = []
        nonflaky_data = []
        for flaky_test in flaky_tests:
            flaky_data.append(
                calc_average_distance(
                    curr_test=flaky_test, tests=flaky_tests, dist_cache=dist_cache)
            )
        for flaky_test in flaky_tests:
            nonflaky_data.append(
                calc_average_distance(
                    curr_test=flaky_test, tests=nonflaky_tests, dist_cache=dist_cache)
            )
        # for nonflaky_test in nonflaky_tests:
        #     nonflaky_data.append(
        #         calc_average_distance(
        #             curr_test=nonflaky_test, tests=nonflaky_tests, dist_cache=dist_cache)
        #     )
        #     # nonflaky_data.append(0)
        # store data in master list
        if len(flaky_data) > flaky_test_req:
            flaky.append(flaky_data)
            nonflaky.append(nonflaky_data)
            project_names.append(project.name)
    
    # plot data
    plot_boxplot(
        project_names=project_names,
        left_data=flaky,
        right_data=nonflaky,
        title='{}: Comparison of LCS score in AST'.format(dataset),
        ylabel='longest common subsequence score',
        label_left='flaky v. flaky',
        label_right='flaky v. nonflaky'
    )

if __name__ == '__main__':
    target = 'flast'
    projects, tests = load_data(
        input_file='data/extracted-{}.csv'.format(target),
        output_file='data/{}.pickle'.format(target),
        overwrite=False,
    )
    min_flaky_tests=3
    # for project in projects.values():
    #     plot_top_n_methods(
    #         dataset='{}-{}'.format(target, project.name),
    #         tests=project.tests,
    #         unique=False)
    # plot_top_n_methods(dataset=target, tests=tests, unique=False)
    # plot_flaky_tests(
    #     dataset=target,
    #     projects=projects, 
    #     flaky_test_req=min_flaky_tests,
    # )
    # plot_num_nodes(
    #     dataset=target,
    #     projects=projects, 
    #     flaky_test_req=min_flaky_tests,
    # )
    # plot_num_methods(
    #     dataset=target,
    #     projects=projects, 
    #     # category_filter=interested_flaky_categories,
    #     flaky_test_req=min_flaky_tests,
    # )
    # plot_num_methods(
    #     dataset=target,
    #     projects=projects, 
    #     unique=False, 
    #     # category_filter=interested_flaky_categories, 
    #     flaky_test_req=min_flaky_tests,
    # )
    plot_average_distances(
        dataset=target,
        projects=projects,
        flaky_test_req=3,
        flaky_limit=400,
        nonflaky_limit=2000,
    )
