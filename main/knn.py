import os
import pickle
import time
import zss
import numpy as np
import warnings
import heapq as hq
from analyze import load_data
from analyze import apted_edit_distance
from analyze import longest_common_subsequence
from filter import filter_tests
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import precision_score, recall_score
from sklearn.model_selection import StratifiedKFold

def get_neighbours(current_test, train_data, num_neighbours, dist_cache):
    # max_heap = []
    # hq.heapify(max_heap)
    # for other_test in train_data:        
    #     if other_test._id != current_test._id:
    #         if len(max_heap) == num_neighbours:
    #             # check max heap for cutoff (avoid doing costly edit distance computation)
    #             node_diff = abs(other_test.num_nodes - current_test.num_nodes)
    #             if node_diff > -1 * max_heap[0][0]:
    #                 continue
    #         dist = apted_edit_distance(current_test, other_test, dist_cache)
    #         # update max heap
    #         hq.heappush(max_heap, (-dist, other_test))
    #         if len(max_heap) > num_neighbours:
    #             hq.heappop(max_heap)
    # return [(-elem[0], elem[1]) for elem in max_heap]

    # old method
    distances = []
    for other_test in train_data:
        if other_test._id != current_test._id:
            # dist = apted_edit_distance(current_test, other_test, dist_cache)
            lcs = longest_common_subsequence(current_test, other_test, lcs_cache=dist_cache)
            dist = 1.0 - (lcs/other_test.num_nodes)
            distances.append((dist, other_test))
    distances.sort(key=lambda tup: tup[0])
    return distances[:num_neighbours]

def predict_flaky(current_test, train_data, num_neighbours, sigma, dist_cache):
    neighbours = get_neighbours(
        current_test=current_test, 
        train_data=train_data, 
        num_neighbours=num_neighbours,
        dist_cache=dist_cache,
    )
    # numFlaky = sum([(1 if n.is_flaky else 0) for dist, n, in neighbours])
    # return numFlaky >= num_neighbours / 2
    
    phi, psi = 0, 0
    for distance, neighbour in neighbours:
        dInv = (1/distance) if distance != 0 else float('inf')
        if neighbour.is_flaky:
            phi += dInv
        else:
            psi += dInv
    # handle limit cases for prediction
    if phi == float("Inf") and psi == float("Inf"):
        return False
    elif psi == float("Inf"):
        return False
    elif phi == float("Inf"):
        return True
    elif (phi + psi) == 0:
        return False
    else:
        if phi / (phi + psi) >= sigma:
            return True
        else:
            return False

def predict_flaky_alt(current_test, train_flaky_tests, sigma, dist_cache):
    for test in train_flaky_tests:
        if test.is_flaky:
            if apted_edit_distance(current_test, test, dist_cache) < sigma:
                return True
    return False

def knn_classification(train_data, test_data, k, sigma, dist_cache):
    # 
    predictions = []
    t0 = time.perf_counter()
    for test in test_data:
        prediction = predict_flaky(
            current_test=test, 
            train_data=train_data, 
            num_neighbours=k, 
            sigma=sigma,
            dist_cache=dist_cache)
        predictions.append(prediction)
    t1 = time.perf_counter()
    test_time = t1 - t0

    return test_time/len(test_data), predictions

def compute_results(testLabels, predictLabels):
    # compute metrics
    res = {}
    warnings.filterwarnings("error")  # to catch warnings, e.g., "prec set to 0.0"
    try:
        precision = precision_score(testLabels, predictLabels)
    except:
        precision = "-"
    try:
        recall = recall_score(testLabels, predictLabels)
    except:
        recall = "-"
    warnings.resetwarnings()  # warnings are no more errors
    return precision, recall

if __name__ == "__main__":
    target = 'flast'
    v0 = time.perf_counter()
    projects, tests = load_data(
        input_file='data/extracted-{}.csv'.format(target),
        output_file='data/{}.pickle'.format(target),
        overwrite=True,
    )
    v1 = time.perf_counter()
    print('load time: {}'.format(v1-v0))

    project_list = [
        # 'achilles',
        "alluxio-tachyon",
        # "ambari",
        "hadoop",
        # "jackrabbit-oak",
        "jimfs",
        "ninja",
        "okhttp",
        # "oozie",
        "oryx",
        # "spring-boot",
        "togglz",
        "wro4j",
    ]
    interested_flaky_categories = set([
        # 'ID',
        # 'NOD', 
        # 'NDOI',
        # 'UD',
        # None,
    ])

    outDir = "data/"
    outFile = "{}-lcs-knn.csv".format(target)
    os.makedirs(outDir, exist_ok=True)
    with open(os.path.join(outDir, outFile), "w") as fo:
        fo.write("dataset,flakyTrain,nonFlakyTrain,flakyTest,nonFlakyTest,k,sigma,precision,recall,preparationTime,predictionTime\n")

    num_k_folds = 5
    min_flaky_test = 0
    kf = StratifiedKFold(n_splits=num_k_folds, shuffle=True, random_state=0)
    for k in [3]:
        for sigma in [.5, .75, .95]:
            overallP, overallR = 0, 0
            counter = 0
            for project in projects.values():
                if len(project_list) > 0 and project.name not in project_list:
                    continue
                
                # gather project data
                tests = list(project.tests.values())
                tests = filter_tests(tests, interested_flaky_categories)
                node_counts = np.array([test.num_nodes for test in tests])
                invoked_methods_counts= np.array([len(test.invoked_methods) for test in tests])
                X = np.array(tests)
                Y = np.array([test.is_flaky for test in tests])
                num_flakies = Y[Y == True].shape[0]
                print('\nproject: {}'.format(project.name))
                if num_flakies < k/2 or num_flakies < min_flaky_test:
                    print('not enough flakies tests, skipping project...')
                    continue
                print('total tests: {}'.format(X.shape[0]))
                print('flaky tests: {}'.format(num_flakies))
                print('nonflaky tests: {}'.format(len(tests) - num_flakies))
                print('ave node count: {}'.format(np.average(node_counts)))
                print('ave invoked methods count: {}'.format(np.average(invoked_methods_counts)))

         
                
                # create cache for project
                dist_cache = dict()
                v0 = time.perf_counter()
                avgP, avgR = 0, 0
                avgTPrep, avgTPred = 0, 0
                avgFlakyTrain, avgNonFlakyTrain, avgFlakyTest, avgNonFlakyTest = 0, 0, 0, 0
                successFold = 0
                for kFold, (train_indices, test_indices) in enumerate(kf.split(X, Y)):
                    train_data, train_labels = X[train_indices], Y[train_indices]
                    test_data, test_labels = X[test_indices], Y[test_indices]
                    if sum(train_labels) == 0 or sum(test_labels) == 0:
                        print("Skipping fold...")
                        print(" Flaky Train Tests", sum(train_labels))
                        print(" Flaky Test Tests", sum(test_labels))
                        continue
                    
                    avgFlakyTrain += sum(train_labels)
                    avgNonFlakyTrain += len(train_labels) - sum(train_labels)
                    avgFlakyTest += sum(test_labels)
                    avgNonFlakyTest += len(test_labels) - sum(test_labels)

                    prediction_time, predictions = knn_classification(train_data, test_data, k, sigma, dist_cache)
                    precision, recall = compute_results(test_labels, predictions)
                    if precision != "-" and recall != "-":
                        print("{:0.3f} {:0.3f}".format(precision, recall))
                    elif precision == "-":
                        print("{} {:0.3f}".format(precision, recall))
                    elif recall == '-':
                        print("{:0.3f} {}".format(precision, recall))
                    
                    if precision != "-" and recall != "-":
                        successFold += 1
                        avgP += precision
                        avgR += recall
                        avgTPred += prediction_time
                v1 = time.perf_counter()
                print('k-fold: {}'.format(v1-v0))

                if successFold == 0:
                    avgP = "-"
                    avgR = "-"
                    avgTPrep = "-"
                    avgTPred = "-"
                    avgFlakyTrain = "-"
                    avgNonFlakyTrain = "-"
                    avgFlakyTest = "-"
                    avgNonFlakyTest = "-"
                else:
                    avgP /= successFold
                    avgR /= successFold
                    counter += 1
                    avgTPrep = '-'
                    avgTPred /= successFold
                    avgFlakyTrain /= successFold
                    avgNonFlakyTrain /= successFold
                    avgFlakyTest /= successFold
                    avgNonFlakyTest /= successFold

                with open(os.path.join(outDir, outFile), "a") as fo:
                    fo.write("{},{},{},{},{},{},{},{},{},{},{}\n".format(project.name, avgFlakyTrain, avgNonFlakyTrain, avgFlakyTest, avgNonFlakyTest, k, sigma, avgP, avgR, avgTPrep, avgTPred))
                if avgP != '-':
                    overallP += avgP
                if avgR != '-':
                    overallR += avgR
            print('k={}, sigma={}, P={}, R={}'.format(k, sigma, overallP/counter, overallR/counter))