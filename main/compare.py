import plot_utils
import pandas as pd


if __name__ == '__main__':
    data_dir = './data'
    csvs = {
        'FLAST':'eff-eff.csv',
        'Random Forest': 'flast-tfidf-randforest.csv',
        'Tree Edit Dist.': 'flast-edit-dist-knn.csv',
    }
    dfs = {}
    for csv_name in csvs:
        csv = csvs[csv_name]
        df = pd.read_csv('{}/{}'.format(data_dir, csv))
        df = df[df.precision != '-']
        dfs[csv_name] = df

    
    precision = []
    recall = []
    projects = []
    for csv_name in dfs:
        projects.append(csv_name)
        precisions = [float(p) for p in dfs[csv_name]['precision'].tolist()]
        precision.append(precisions)
        recalls = [float(r) for r in dfs[csv_name]['recall'].tolist()]
        recall.append(recalls)

    plot_utils.plot_boxplot(
        project_names=projects,
        left_data=precision,
        right_data=recall,
        title='Approach Comparison',
        label_left='precision',
        label_right='recall',
        color_right='green',
        color_left='blue',
        xtick_rotation=0,
        ha='center',
        figsize=(8,4)
    )