import pandas as pd
import shutil
import os
import pprint
import javalang
from models import Project
from models import UnitTest
from extract_utils import read_test_file_content
from extract_utils import extract_raw_test_methods
from extract_utils import extract_method_body
from extract_utils import cols

data_dir = './data/dataset'
output_file = './data/extracted-flast.csv'

def extract_flast_data():
    i = 0
    limit = float('inf')
    skipped_tests = 0
    duplicate_ids = 0
    total = 0
    # delete output file
    if os.path.exists(output_file):
        os.remove(output_file)
    project_dirs = os.listdir(data_dir)
    for project_dir in project_dirs:
        print('\nproject: {}'.format(project_dir))
        project = Project(
            name=project_dir
        )
        # flaky tests
        flaky_tests = os.listdir('{}/{}/flakyMethods'.format(
            data_dir, project_dir))
        flaky_tests_set = set(flaky_tests)
        # sanity check
        print('duplicate flaky tests: {}'.format(
            len(flaky_tests) - len(flaky_tests_set)))
        total += len(flaky_tests)

        for flaky_test in flaky_tests:
            file_path = '{}/{}/flakyMethods/{}'.format(
                data_dir, project_dir, flaky_test)
            file_content = read_test_file_content(file_path)
            parsable_content = 'public class MyClass {{ {} }}'.format(
                file_content)
            raw_test = extract_raw_test_methods(file_content)
            test_name = flaky_test.split('.')[-1]
            if test_name in raw_test:
                new_test = UnitTest(
                    project_name=project.name,
                    full_name=flaky_test,
                    is_flaky=True,
                    raw=raw_test[test_name]
                )
                project.add_test(new_test)
        
        # nonflaky tests
        nonflaky_tests = os.listdir('{}/{}/nonFlakyMethods'.format(
            data_dir, project_dir))
        nonflaky_tests_set = set(nonflaky_tests)
        # sanity check
        print('duplicate nonflaky tests: {}'.format(
            len(nonflaky_tests) - len(nonflaky_tests_set)))
        total += len(nonflaky_tests)
        # for flaky_test in flaky_tests_set:
        #     if flaky_test in nonflaky_tests_set:
        #         # print('flaky & nonflaky at the same time! {}'.format(flaky_test))
        #         duplicate_ids += 1
        for nonflaky_test in nonflaky_tests:
            file_path = '{}/{}/nonFlakyMethods/{}'.format(
                data_dir, project_dir, nonflaky_test)
            file_content = read_test_file_content(file_path)
            parsable_content = 'public class MyClass {{ {} }}'.format(
                file_content)
            raw_test = extract_raw_test_methods(file_content)
            test_name = nonflaky_test.split('.')[-1]
            if test_name in raw_test:
                new_test = UnitTest(
                    project_name=project.name,
                    full_name=nonflaky_test,
                    is_flaky=False,
                    raw=raw_test[test_name]
                )
                project.add_test(new_test)

        output_list = [] 
        for _id in project.tests:
            test = project.tests[_id]
            if test.raw == None or len(test.raw) == 0:
                skipped_tests += 1
                # print('{} has invalid raw: {}'.format(test.full_name, test.raw))
                continue
            output_list.append(test.to_row())
        out_df = pd.DataFrame(output_list, columns=cols)
        try:
            out_df.to_csv(output_file, 
                mode='a', index=False, header=(i==0), encoding='utf-8')
        except UnicodeEncodeError as e:
            print(e)
        i+=1
        if i >= limit:
            break
    return total, skipped_tests, duplicate_ids

if __name__ == '__main__':
    total, skipped_tests, duplicate_ids = extract_flast_data()
    print('total: {}'.format(total))
    print('skipped tests: {}'.format(skipped_tests))
    print('duplicate ids: {}'.format(duplicate_ids))
    
    df = pd.read_csv(output_file)
    print("output shape: {}".format(df.shape))
    is_flaky = df['is_flaky'] == True
    print("is flaky: {}".format(df[is_flaky].shape))