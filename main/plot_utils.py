import os
import numpy as np
import matplotlib.pyplot as plt

left_color = '#D7191C'
right_color = '#2C7BB6'
default_figsize=(16, 9)

def plot_boxplot(project_names, right_data, left_data, title='', ylabel='', 
        label_right='nonflaky', label_left='flaky', 
        color_right=right_color, color_left=left_color, xtick_rotation=45,
        ha='right', figsize=default_figsize):
    if not os.path.exists('./graphs'):
        os.makedirs('./graphs')
    # plot data
    fig = plt.figure(figsize=figsize)
    bpl = plt.boxplot(right_data, positions=np.array(range(len(right_data)))*2.0-0.4, sym='', widths=0.6)
    bpr = plt.boxplot(left_data, positions=np.array(range(len(left_data)))*2.0+0.4, sym='', widths=0.6)
    set_box_color(bpl, color_left) # colors are from http://colorbrewer2.org/
    set_box_color(bpr, color_right)
    # draw temporary red and blue lines and use them to create a legend
    plt.plot([], c=color_left, label=label_left)
    plt.plot([], c=color_right, label=label_right)
    plt.legend()
    plt.xticks(range(0, len(project_names) * 2, 2), project_names, rotation=xtick_rotation, ha=ha)
    plt.xlim(-2, len(project_names)*2)
    plt.ylabel(ylabel)
    # plt.ylim(0, 8)
    fig.suptitle(title)
    plt.tight_layout()
    if len(title) > 0:
        plt.savefig('./graphs/{}.png'.format(title))
    plt.close()

def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)

def plot_bargraph(project_names, flaky, nonflaky, title='', ylabel=''):
    if not os.path.exists('./graphs'):
        os.makedirs('./graphs')
    x = np.arange(len(project_names))  # the label locations
    width = 0.35  # the width of the bars

    fig, ax = plt.subplots(figsize=default_figsize)
    rects1 = ax.bar(x - width/2, flaky, width, label='flaky', color=left_color)
    rects2 = ax.bar(x + width/2, nonflaky, width, label='nonflaky', color=right_color)

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel(ylabel)
    ax.set_title(title)
    ax.set_xticks(x)
    ax.set_xticklabels(project_names, rotation=45, ha='right')
    ax.tick_params(axis='x', which='major', pad=5)
    ax.legend()

    ax.bar_label(rects1, padding=3)
    ax.bar_label(rects2, padding=3)

    plt.tight_layout()
    if len(title) > 0:
        plt.savefig('./graphs/{}.png'.format(title))
    plt.close()

def plot_horizontal_bargraphs(flaky, nonflaky, xlabel='', title=''):
    if not os.path.exists('./graphs'):
        os.makedirs('./graphs')
    
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(22, 9))
    y_pos = np.arange(len(flaky))
    flaky_values = [f[1] for f in flaky]
    flaky_ticks = [f[0] for f in flaky]
    ax1.barh(y_pos, flaky_values, align='center')
    ax1.set_yticks(y_pos)
    ax1.set_yticklabels(flaky_ticks)
    ax1.invert_yaxis()  # labels read top-to-bottom
    ax1.set_title('flaky tests')
    ax1.set_xlabel(xlabel)

    y_pos = np.arange(len(nonflaky))
    nonflaky_values = [f[1] for f in nonflaky]
    nonflaky_ticks = [f[0] for f in nonflaky]
    ax2.barh(y_pos, nonflaky_values, align='center')
    ax2.set_yticks(y_pos)
    ax2.set_yticklabels(nonflaky_ticks)
    ax2.invert_yaxis()  # labels read top-to-bottom
    ax2.set_title('non-flaky tests')
    ax2.set_xlabel(xlabel)
    fig.suptitle(title)
    if len(title) > 0:
        plt.savefig('./graphs/{}.png'.format(title))
    plt.close()

