import javalang
import zss
import models
from graphviz import Digraph

# Default Config -------------------------------------------------------------------
# set of javalang.tree.Node types to include when constructing the AST
considered_nodes = set([
    javalang.tree.MethodDeclaration,
    javalang.tree.MemberReference,
    javalang.tree.MethodInvocation,
    # javalang.tree.LocalVariableDeclaration,
    # javalang.tree.ForStatement,
    # javalang.tree.ForControl,
    # javalang.tree.EnhancedForControl,
    # javalang.tree.WhileStatement,
    javalang.tree.Annotation,
])
# set of method names to ignore
ignored_methods = set([
    # 'assertArrayEquals'
    # 'assertEquals',
    # 'assertFalse',
    # 'assertNotNull',
    # 'assertNotSame',
    # 'assertNull',
    # 'assertSame',
    # 'assertThat',
    # 'assertTrue',
    # 'is',
    # 'fail',
])
# --------------------------------------------------------------------------

def gen_dft(curr_tree):
    if curr_tree == None:
        return []
    result = [curr_tree.name]
    for child in curr_tree.children:
        result += gen_dft(child)
    return result

def verify_tree(curr_node, visited):
    """
    Verify that graph rooted at specific node is indeed a tree
    """
    curr_label = get_label(curr_node)
    if curr_label in visited:
        return False
    visited.add(curr_label)
    result = True
    for child in curr_node.children:
        result &= verify_tree(child, visited)
        if result == False:
            return result
    visited.remove(curr_label)
    return result

def get_label(node):
    if hasattr(node, 'label'):
        return getattr(node, 'label')
    else:
        return getattr(node, 'name')

def gen_ast_tree(source_code):
    """
    Convert raw source code to ast tree from javalang
    """
    try:
        return javalang.parse.parse(source_code)
    except:
        raise

def gen_zss_tree(source_code,
            javalang_tree=None,
            replace_var_names=True):
    """
    Convert raw source code to tree data structure from zss 
    module (for calc. tree edit distance) and returns the root node the tree 
    """
    # gen abstract synatx tree using javalang first
    if javalang_tree == None:
        try:
            javalang_tree = gen_ast_tree(source_code)
        except:
            raise
    else:
        javalang_tree = javalang_tree
    # convert to zss tree
    return javalang_to_zss_tree(
        curr_node=javalang_tree, 
        parent=None,
        node_cache=dict(),
        node_id_lookup=dict(),
        alias_lookup=dict(),
        type_id_lookup=dict()
    )

def gen_apted_tree(source_code, zss_tree=None):
    if zss_tree == None:
        zss_tree = gen_zss_tree(source_code)
    return zss_to_apted_tree(zss_tree)

def zss_to_apted_tree(zss_node):
    if zss_node == None:
        return None
    children = []
    for child in zss_node.children:
        converted = zss_to_apted_tree(child)
        if converted:
            children.append(converted)
    return models.Node(name=zss_node.label, children=children)


def javalang_to_zss_node(_id, node, alias_lookup, type_id_lookup, node_cache):
    """
    Convert javalang.ast.Node object to zss graphviz.simple_tree.Node
    object. Returns None if node should be ignored
    """
    if isinstance(node, javalang.ast.Node) == False:
        return None
    node_str = javalang_to_str(node, alias_lookup, type_id_lookup)
    if node_str != None:
        node_type = type(node).__name__
        label = '{}-{}'.format(node_type, node_str)
        if label not in node_cache:
            node_cache[label] = zss.Node(label)
        return node_cache[label]
    return None

def javalang_to_str(node, alias_lookup, type_id_lookup):
    if node == None:
        return ''
    str_builder = []

    if isinstance(node, javalang.tree.LocalVariableDeclaration):
        # variable declaration
        declarator_node = getattr(node, 'declarators')[0]
        var_name = getattr(declarator_node, 'name')
        _type_node = getattr(node, 'type')
        if _type_node:
            _type_name = getattr(_type_node, 'name')
            if _type_name not in type_id_lookup:
                type_id_lookup[_type_name] = 0
            if var_name not in alias_lookup:
                alias_lookup[var_name] = '{}Var{}'.format(_type_name, type_id_lookup[_type_name])
                # alias_lookup[var_name] = '{}Var'.format(_type_name)
            type_id_lookup[_type_name] += 1
            str_builder.append('name={}'.format(alias_lookup[var_name]))
    elif isinstance(node, javalang.tree.Type):
        # reference type
        name_val = getattr(node, 'name')
        str_builder.append('type={}'.format(name_val))
    elif isinstance(node, javalang.tree.MethodInvocation):
        # method invocation
        var_name = getattr(node, 'qualifier')
        if var_name and len(var_name) > 0:
            if var_name[0].islower():
                if var_name in alias_lookup:
                    str_builder.append('qualifier={}'.format(alias_lookup[var_name]))
                else:
                    str_builder.append('qualifier=var')
            else:
                str_builder.append('qualifier={}'.format(var_name)) # static method
        # get method name
        method_name = getattr(node, 'member')
        # check for ignored methods
        for ignore_method in ignored_methods:
            if ignore_method == method_name.lower():
                return None
        str_builder.append('method={}'.format(method_name))
    elif isinstance(node, javalang.tree.MemberReference):
        member_name = getattr(node, 'member')
        if member_name in alias_lookup:
            str_builder.append('member={}'.format(alias_lookup[member_name]))
        else:
            str_builder.append('member={}'.format(member_name))
    elif isinstance(node, javalang.tree.MethodDeclaration):
        method_type_key = 'method--def'
        if method_type_key not in type_id_lookup:
            type_id_lookup[method_type_key] = 0 
        # str_builder.append('name=method{}'.format(type_id_lookup[method_type_key]))   
        type_id_lookup[method_type_key] += 1 
    elif isinstance(node, javalang.tree.Annotation):
        name_val = getattr(node, 'name')
        str_builder.append('name={}'.format(name_val))
        element = getattr(node, 'element')
        if element:
            if isinstance(element, list):
                for i in range(len(element)):
                    elem_name_val = getattr(element[i], 'name')
                    elem_value_node = getattr(element[i], 'value')
                    if hasattr(elem_value_node, 'value'):
                        elem_val = getattr(elem_value_node, 'value')
                        str_builder.append('element{}={},value={}'.format(
                            i, elem_name_val, elem_val))
                    elif hasattr(elem_value_node, 'type'):
                        type_val = getattr(elem_value_node, 'type')
                        str_builder.append('element={},type={}'.format(elem_name_val, type_val))
            elif isinstance(element, javalang.tree.Literal):
                elem_val = getattr(element, 'value')
                str_builder.append('value={}'.format(elem_val))


    return ",".join(str_builder)

def javalang_to_zss_tree(curr_node, node_id_lookup, alias_lookup, type_id_lookup, node_cache, 
                        parent=None):
    """
    Convert all javalang.ast.Node objects to zss graphviz.simple_tree.Node
    objects in a given tree
    """
    if curr_node == None:
        return None
    
    is_considered_node = len(considered_nodes) == 0 \
                        or type(curr_node) in considered_nodes
    if isinstance(curr_node, javalang.ast.Node) and is_considered_node:
        # convert current node
        node_type = type(curr_node).__name__
        if node_type not in node_id_lookup:
            node_id_lookup[node_type] = 0
        converted = javalang_to_zss_node(
            _id=node_id_lookup[node_type], 
            node=curr_node,
            node_cache=node_cache,
            alias_lookup=alias_lookup,
            type_id_lookup=type_id_lookup
        )
        if converted:
            # increment id counter
            node_id_lookup[node_type] += 1
            curr_node_id_lookup = dict()
            # convert children
            for child in curr_node.children:
                # convert current child
                converted_child = javalang_to_zss_tree(
                    curr_node=child, 
                    parent=converted, 
                    node_cache=node_cache,
                    node_id_lookup=curr_node_id_lookup,
                    alias_lookup=alias_lookup,
                    type_id_lookup=type_id_lookup
                )
                if converted_child and converted != converted_child and has_child(converted, converted_child) == False:
                    # convert successful
                    converted.addkid(converted_child)
            return converted
    
    # Note: javalang.ast.Node may contain children which are not javalang.ast.Node
    # objects, so need to check for some special cases when traversing the tree
    if isinstance(curr_node, javalang.ast.Node):
        children = curr_node.children
    elif isinstance(curr_node, (list, tuple)):
        children = curr_node
    else:
        return None

    for child in children:
        if isinstance(child, (javalang.ast.Node, list, tuple)):
            converted_child = javalang_to_zss_tree(
                curr_node=child, 
                parent=parent,
                node_cache=node_cache,
                node_id_lookup=node_id_lookup,
                alias_lookup=alias_lookup,
                type_id_lookup=type_id_lookup
            )
            if parent == None and converted_child != None:
                parent = converted_child
            if converted_child and parent != converted_child and has_child(parent, converted_child) == False:
                # convert successful
                parent.addkid(converted_child)
    return parent

def has_child(parent, child):
    result = False
    for c in parent.children:
        if c.label == child.label:
            result = True
            break
    return result

def print_tree(curr_node, indent=0):
    if curr_node == None:
        return
    print((indent*'-')+get_label(curr_node))
    for child in curr_node.children:
        print_tree(child, indent+1)

def javalang_node_count(curr_node):
    if curr_node == None:
        return 0
    if isinstance(curr_node, javalang.ast.Node):
        count = 1
        for child in curr_node.children:
            count += javalang_node_count(child)
        return count
    else:
        count = 0
        for child in curr_node:
            if isinstance(child, (javalang.ast.Node, list, tuple)):
                count += javalang_node_count(child)
        return count

def node_count(curr_node, visited):
    if curr_node == None:
        return 0
    elif get_label(curr_node) in visited:
        return 0
    count = 1
    visited.add(get_label(curr_node))
    for child in curr_node.children:
        count += node_count(child, visited)    
    return count

def depth_first_traversal_list(curr_node):
    if curr_node == None:
        return []
    result = [curr_node]
    for child in curr_node.children:
        result += depth_first_traversal_list(child)
    return result

def get_invoked_methods(curr_node):
    """
    Returns list of method names that are invoked within a javalang
    abstract syntax tree.
    """
    if curr_node == None:
        return []
    methods = []
    if isinstance(curr_node, javalang.ast.Node):
        # convert current node
        if isinstance(curr_node, javalang.tree.MethodInvocation):
            is_ignored = False
            method_name = getattr(curr_node, 'member')
            for ignore_method in ignored_methods:
                if ignore_method in method_name.lower():
                    is_ignored = True
                    break
            if is_ignored == False:
                methods.append(method_name)
        
        for child in curr_node.children:
            # convert current child
            methods += get_invoked_methods(child)
    elif isinstance(curr_node, (list, tuple)):
        for child in curr_node:
            if isinstance(child, (javalang.ast.Node, list, tuple)):
                methods += get_invoked_methods(child)
    return methods

def add_edges(current_node, graph, visited):
    """
    Add edges from current node to all of its children
    to the graph
    """
    if current_node == None:
        return
    curr_label = get_label(current_node)
    for child in current_node.children:
        child_label = get_label(child)
        edge = curr_label+'-'+child_label
        if edge not in visited:
            visited.add(edge)
        else:
            continue
        # add edge from parent to child
        graph.edge(curr_label, child_label)
        # add edges in child
        add_edges(child, graph, visited)

def visualize_tree(root, name='t'):
    """
    Displays visualization of tree
    """
    import os 
    if not os.path.exists('./trees'):
        os.makedirs('./trees')
    g = Digraph(name, 
        filename='./trees/{}.gv'.format(name),
        format='png'
    )
    g.attr('node', shape='box')
    add_edges(root, g, set())
    u = g.unflatten(stagger=2)
    u.render()

def visualize_traversal(node_list, name='trav'):
    import os 
    if not os.path.exists('./trees'):
        os.makedirs('./trees')
    g = Digraph(name, 
        filename='./trees/{}.gv'.format(name),
        format='png'
    )
    g.attr('node', shape='box')
    for i in range(len(node_list) - 1):
        g.edge(get_label(node_list[i]), 
            get_label(node_list[i+1]))
    u = g.unflatten(stagger=2)
    u.render()
