# static-flaky-tests

## Project setup
1. Create/activate a python virtual env.
2. Run `pip install -r requirements.txt` from project root directory

## Data extraction
1. Go to `./data` directory
2. Run `python extract.py`
3. Output in `./data/data.csv`